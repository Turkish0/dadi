import random as r

#print(r.randint(0,5))
def tiradado(dado, numero):
    dado=int(dado)
    numero=int(numero)
    somma=0
    while (numero > 0):
        tiro = (r.randint(0,30)%(dado+1))+1
        print(str(tiro)+ ",", end="")
        numero=numero-1
        somma=somma+tiro
    print()
    print("Somma: " + str(somma))
    return somma

def variazione(somma):
    scelta=input("Vuoi aumentare o diminuire? +/-    :")
    perc=input("Di che percentuale?")
    perc=float(perc)
    vari=(somma*perc)/100
    if(scelta == "+"):
        somma=somma+vari
    else:
        somma=somma+vari
    return somma

def main():
    dado=1
    while(dado!=-1):
        dado=input("Che dado vuoi tirare?Scrivere -1 per uscire")
        numero=input("Quante volte vuoi tirarlo?")
        dado=int(dado)
        numero=int(numero)
        somma=tiradado(dado,numero)
        scelta=input("Vuoi varirare la somma di una percentuale? y/n    :")
        if(scelta.lower()=="y"):
            newsomma=variazione(somma)
            print("Nuova somma: " + str(newsomma))
        print()
        print()


if __name__ == '__main__':
    main()
